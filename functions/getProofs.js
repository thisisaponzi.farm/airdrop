const fs = require('fs');
let rawdata = fs.readFileSync('data/merkle-proof.generated.json');
let logs = JSON.parse(rawdata);

const returnProof = (addr) => {
    try {
        return {
            index: logs['claims'][addr]['index'],
            proof: logs['claims'][addr]['proof'],
            amount: logs['claims'][addr]['amount']
        }
    }
    catch(e){
        return {
            proof: ['does_not_exist']
        }
    }

}


exports.handler = async function(event, context) {
    return {
        statusCode: 200,
        body: JSON.stringify({res: returnProof(event.queryStringParameters.address) })
    };
}