import logo from './logo.svg';
import './App.css';
import React from 'react';
const rawdata = require('./merkle-proof.generated.json')
let logs = JSON.parse(rawdata);

const returnProof = (addr) => {
  try {
      return {
          index: logs['claims'][addr]['index'],
          proof: logs['claims'][addr]['proof'],
          amount: logs['claims'][addr]['amount']
      }
  }
  catch(e){
      return {
          proof: ['does_not_exist']
      }
  }

}
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: '', text: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('A address was submitted: ' + this.state.value);
    this.setState({text: returnProof(this.state.value)})
    event.preventDefault();
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Address:
          <input type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
        Proofs: 
        <p>{this.state.text}</p>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}

export default App;
