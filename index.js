const test = "0x31573f6F29aaF7D7fdc3BAeFA2E6628b693Cc340"

'use strict';

const fs = require('fs');

let rawdata = fs.readFileSync('./merkle-proof.generated.json');
let logs = JSON.parse(rawdata);

const returnProof = (addr) => {
    return {
        index: logs['claims'][addr]['index'],
        proof: logs['claims'][addr]['proof'],
        amount: logs['claims'][addr]['amount']
    }
}

console.log(returnProof(test))